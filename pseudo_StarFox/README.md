***Le projet est en cours de développement***

Pour lancer le jeu, exécuter le fichier bin/glbase.

Veuillez noter que le fichier a été compilé avec gcc sur Linux Mint.

Le projet inclu:

* la définition des primitives du cylindre et de la pyramide sommet par sommet
* création des modèles du joueur et des 2 ennemis à partir des primitives en 
  utilisant les transformations matricielles
* animation du joueur selon les contrôles (WASD)
* collision joueur-monde


Le positionnement de la caméra et la projection ainsi que la détection des 
touches étaient fournis.

Travail codé en C++ avec OpenGL.
