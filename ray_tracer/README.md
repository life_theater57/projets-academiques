Les images ont été produites par lancé de rayon récursifs. 
Chacune porte un nom correspondant à la caractéristique implémentée.

Fonctionnalités implémantées:

* intersection avec cube, sphère, cylindre
* transformations de base
* matériau Lambertien
* matériau Phong
* matériau réflectif
* matériau réfractif
* lumière directionnelle
* ombres
* matériaux texturés
* suréchantillonnage
* suréchantillonnage jittered

La structure d'affichage était fournie.

Travail codé en C++ avec OpenGL.
