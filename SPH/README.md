Pour lancer la simulation de fluide, exécuter le fichier SPH.

Instructions d'utilisation:

* clique gauche + déplacement : rotation visuelle du volume
* clique gauche + espace + déplacement : rotation physique du volume
* clique droit + déplacement vertical : zoom
* 0 : mise à 0 de la vitesse des particules
* p : pause
* d : rendu par points
* w : rendu par wireframe


Veuillez noter que le fichier a été compilé avec gcc sur Linux Mint.
De plus, une vidéo de démonstration montre l'essentiel du projet.

Le projet inclu:

* implémentation du mouvement des particules
* gestion de collisions entre les particules et le contenant
* rendu d'une surface via "marching tetrahedra"

La fenêtre, l'intersection avec le contenant, le calcul des forces, le rendu des
particules ainsi que la détection des touches étaient fournis.

Travail codé en C++ avec OpenGL.
