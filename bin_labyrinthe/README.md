Pour exécuter le programme, exécuter le programme en ligne de commande comme suit:
java Laby arg1 arg2 arg3 arg4 arg5

* arg1: la hauteur du labyrinthe en nombre de cases
* arg2: la largeur du labyrinthe en nombre de cases
* arg3: la "densité" sous forme de probabilité [0,1] de générer un muret
* arg4: le nombre de secondes d'affichage du labyrinthe complet avant qu'il ne devienne invisible.
* arg5: le nombre de vies dont dispose votre personnage

