Ce dépôt contients différents projets à effectuer dans le cadre de différents cours. 
Le code source n'est pas fourni pour éviter le plagiat, mais pourrait être fourni en cas de demande.

Les projets SPH, ray_tracer et pseudo_StarFox ont été produits dans un cours d'infograpie.
Les projets bin_tron et bin_labyrinthe contiennent ont été faits dans un cours de programmation 
orientée objet.

Description:

* SPH : Simulation de fluide par "Smoothed Particule Hydrodynamics".
* ray_tracer : Système de lancer de rayons de base.
* pseudo_Starfox : Ébauche d'un jeu simple inspiré du classique Star Fox.
* bin_tron : Jeu inspiré de l'arcade Tron du film Tron.
* bin_labyrithe : Labyrinthe en ligne de commande qui aparait une temps donné en argument avant d'être
                  exploré par l'utilisateur. Le programme inclue un solutionneur automatisé avec 
                  algorithme aléatoire ou récursif.

